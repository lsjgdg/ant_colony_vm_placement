const peromon = require("./peromon.js");
const Solution = require( "./solution.js");
const pick = require("pick-random-weighted");

let maxVmId = -1; //vmID는 0부터 시작
let maxPmId = -1;

let peromonSquareFactor = 1;
let heuristicSquareFactor = 2;

async function acs(_vmList, _pmList, nCycle, antCount, logger, parameters){
    let peromonEvapolation = 0.7;
    let peromonMinFactor = 2;
    let initMaxPeromon = 3;

    if(parameters !== undefined){
        peromonSquareFactor = parameters.peromonSquareFactor !== undefined ? parameters.peromonSquareFactor : 1
        heuristicSquareFactor = parameters.heuristicSquareFactor !== undefined ? parameters.heuristicSquareFactor : 1
        peromonEvapolation = parameters.peromonEvapolation !== undefined ? parameters.peromonEvapolation : 0.7
        peromonMinFactor = parameters.peromonMinFactor !== undefined ? parameters.peromonMinFactor : 2
        initMaxPeromon = parameters.initMaxPeromon !== undefined ? parameters.initMaxPeromon : 3
    }

    peromon.setParameters(peromonEvapolation, peromonMinFactor);

    //각종 초기화
    let vmList = arrayDeepCopy(_vmList);
    let pmList = arrayDeepCopy(_pmList);
    let globalBestSolution = null;
    let peromonMatrix = peromon.getMatrix(vmList, pmList);
    peromon.setAll(peromonMatrix, initMaxPeromon); //원래 페로몬 max로 뿌려야 하지만 현재 구해진 해답이 없어서 그냥 1으로 뿌림
    //초기화 구역 끝


    for(let q = 0; q<nCycle; q++){ //알고리즘 반복 횟수
        let solutionList = [];
        for(let ant=0; ant < antCount; ant++){//각 개미마다
            //개미 초기화 구역
            let nowVmList = arrayDeepCopy(vmList);
            let nowPmIndex = 0
            let nowPm = pmList[nowPmIndex];
            let solution = new Solution(vmList, pmList);
            //개미 초기화 구역 끝

            while(nowVmList.length > 0){//모든 VM을 할당할 때까지 계속
                //아직 할당이 안 되었고, 현재 bin(binIndex)에 그 vm을 할당했을 때 수용량이 폭발하지 않는 모든 vm을 구한다.
                let candiateVmList = findCandiateVm(nowVmList, nowPm, solution);
                // let candiateVmList = [];

                if(candiateVmList.length > 0){
                    //선정된 VM이 있다면
                    let selectedVM = selectVmFromCandidateListWithPeromon(candiateVmList, nowPm, peromonMatrix, solution);//확률 함수에 따라 최종 VM 선택
                    solution.allocate(selectedVM, nowPm);
                    nowVmList = removeItem(nowVmList, selectedVM);
                }
                else {
                    //현재 pm에는 넣을 수 없으니 새 pm을 깐다.
                    nowPmIndex += 1;
                    nowPm = pmList[nowPmIndex];
                }
            }
            //개미 한 마리의 연산 끝
            solutionList.push(solution);
        }

        //모든 개미의 연산 끝

        //목적 함수에 따라 이번 라운드의 최고의 답을 계산한다.
        let roundBestSolution = undefined;
        let isFirst = true;
        for(let so of solutionList){
            if(isFirst){
                isFirst = false;
                roundBestSolution = so;
                continue;
            }

            if(so.isBetterThan(roundBestSolution)){
                roundBestSolution = so;
            }
        }

        if(q === 0 || roundBestSolution.isBetterThan(globalBestSolution)){
            globalBestSolution = roundBestSolution
        }
        logger.log(`${q+1}번 라운드 최고의 답: pm ${roundBestSolution.usedPmCount()}개, global은 ${globalBestSolution.usedPmCount()}`);

        //페로몬 업데이트
        for(let vm of vmList){
            for(let pm of pmList){
                peromon.update(peromonMatrix, vm, pm, globalBestSolution);
            }
        }
        logger.pushPeromon(peromonMatrix);

        if(isQuaterPoint(q, nCycle)){
            await logger.sendSingleLog(`${q+1}번 반복이 방금 끝났습니다.`);
        }
    }

    logger.log(`global 답: pm ${globalBestSolution.usedPmCount()}개`)
    return globalBestSolution;
}

function makeVm(resourceVector){
    maxVmId++;
    return {
        id: maxVmId,
        resources: resourceVector
    }
}

function makePm(capacityVector){
    maxPmId++;
    return {
        id: maxPmId,
        capacity: capacityVector,
    }
}

function arrayDeepCopy(array){
    return JSON.parse(JSON.stringify(array));
}

function arraySum(array1, array2) {
    if(array1 === null){
        return array2;
    }
    if(array2 === null){
        return array1
    }
    if(array1.length !== array2.length){
        throw new Error(`array sum 실패, 두 array의 길이가 다릅니다.`);
    }

    let length = array1.length;
    let sumArray = [];

    for(let i = 0; i<length; i++){
        sumArray.push(array1[i] + array2[i]);
    }

    return sumArray;
}

//array1 - array2
function arrayDiff(array1, array2){
    if(array1.length !== array2.length){
        throw new Error(`array difference 실패, 두 array의 길이가 다릅니다.`);
    }

    let length = array1.length;
    let diffArray = [];

    for(let i = 0; i<length; i++){
        diffArray.push(array1[i] - array2[i]);
    }

    return diffArray;
}

//주어진 vm이 요구하는 총합 자원 요구량을 계산
function totalResources(vmList){
    let sum = [];
    //sumvector 초기화
    if(vmList.length > 0){
        for(let i=0; i<vmList[0].resources.length; i++){
            sum.push(0);
        }
    }
    else{
        sum = null;
    }
    for(let vm of vmList){
        sum = arraySum(sum, vm.resources);
    }
    return sum;
}

function removeItem(vmList, vm){
    let newVmList = [];
    for(let v of vmList){
        if(v.id !== vm.id){
            newVmList.push(v);
        }
    }

    return newVmList;
}

//주어진 vmList에서 pm에 할당 가능한 모든 vm을 구합니다.
function findCandiateVm(vmList, pm, solution){
    let candiateVmList = [];
    let nowUsedResourcesInPm = totalResources(solution.getAllVmInPm(pm));
    for(let vm of vmList){
        let testResourceVector = arrayDeepCopy(nowUsedResourcesInPm);
        testResourceVector = arraySum(testResourceVector, vm.resources);

        let overflowFlag = false;
        for(let i=0; i<testResourceVector.length; i++){
            if(testResourceVector[i] > pm.capacity[i]){
                //단 한 종류의 자원이라도 수용량을 넘었다면
                overflowFlag = true;
            }
        }

        if(overflowFlag === false){
            candiateVmList.push(vm);
        }
    }

    return candiateVmList;
}

function selectVmFromCandidateListWithPeromon(vmList, targetPm, peromonMatrix, solution){
    let probabilityList = [];
    let i=0;
    for(let vm of vmList){
        probabilityList.push([vm, probacilisticDecisionRule(vm, vmList, targetPm, peromonMatrix, solution)]);
        i++;
    }

    let selectedVm = pick(probabilityList);

    return selectedVm;
}

function probacilisticDecisionRule(vm, vmList, pm, peromonMatrix, solution){
    let upper = Math.pow(peromonMatrix[vm.id][pm.id], peromonSquareFactor)*Math.pow(heuristricInformation(vm, pm, solution), heuristicSquareFactor);
    let lower = 0;
    for(let otherVm of vmList){
        lower += Math.pow(peromonMatrix[otherVm.id][pm.id], peromonSquareFactor)*Math.pow(heuristricInformation(otherVm, pm, solution), heuristicSquareFactor);
    }

    return upper / lower;
}

function heuristricInformation(vm, pm, solution){
    let capacityOfPm = pm.capacity;
    let nowUsedResourcesInPm = totalResources(solution.getAllVmInPm(pm));
    let totalResourceDemand = arraySum(nowUsedResourcesInPm, vm.resources);

    let l1Distance = L1Distance(arrayDiff(capacityOfPm, totalResourceDemand));

    if(l1Distance === 0){
        return 99999;
    }

    return 1/l1Distance;
}

function L1Distance(vector){
    let distance = 0;

    for(let element of vector){
        distance += Math.abs(element);
    }

    return distance;
}


function makeLogger(){
    return {
        logList: [],
        log(str){
            console.log(str);
            this.logList.push(str);
        }
    }
}

//4분위 지점이면 true 반환
//iterationNumber는 0부터 totalIteration-1까지
function isQuaterPoint(iterationNumber, totalIteration){
    let quater = Number.parseInt(totalIteration/4);
    let quater1 = 0;
    let quater2 = quater-1;
    let quater3 = quater*2-1;
    let quater4 = quater*3-1;

    if(iterationNumber == quater1){
        return true;
    }
    if(iterationNumber == quater2){
        return true;
    }
    if(iterationNumber == quater3){
        return true;
    }
    if(iterationNumber == quater4){
        return true;
    }

    return false;
}

function clearIdState(){
    maxVmId = -1;
    maxPmId = -1;
}

module.exports = {
    acs,
    makeVm,
    makePm,
    makeLogger,
    clearIdState
}

/*
notation 번역
IS -> nowVMList
I -> vmList
v -> binIndex
 */