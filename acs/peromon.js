const objectiveFunction = require("./objectiveFunction.js");

let evapolationDegree = 0.7; //휘발도
let minFactor = 2; //peromon min에 미치는 영향도 1보다 커야 함

function getMatrix(vmList, pmList){
    let vmLength = vmList.length;
    let pmLength = pmList.length;

    let matrix = [];

    for(let i=0; i<vmLength; i++){
        matrix[i] = [];
        for(let j=0; j<pmLength; j++){
            matrix[i][j] = 0;
        }
    }

    return matrix;
}

function setAll(peromonMatrix, value){
    let vmLength = peromonMatrix.length;
    let pmLength = peromonMatrix[0].length;

    for(let i=0; i<vmLength; i++){
        for(let j=0; j<pmLength; j++){
            peromonMatrix[i][j] = value;
        }
    }
}

function calMax(bestSoluction){
    return 1/ (objectiveFunction(bestSoluction)*(1-evapolationDegree));
}

function calMin(bestSoluction){
    return calMax(bestSoluction) / minFactor;
}

function update(peromonMatrix, vm, pm, bestSoluction){
    let vmIndex = vm.id;
    let pmIndex = pm.id;

    let updateValue = 0;
    if(bestSoluction.isContain(vm, pm)){
        updateValue = 1/objectiveFunction(bestSoluction);
    }

    //페로몬 업데이트 규칙
    peromonMatrix[vmIndex][pmIndex] = (1-evapolationDegree)*peromonMatrix[vmIndex][pmIndex] + updateValue;

    let max = calMax(bestSoluction);
    let min = calMin(bestSoluction);

    if(peromonMatrix[vmIndex][pmIndex] > max){
        peromonMatrix[vmIndex][pmIndex] = max;
    }

    if(peromonMatrix[vmIndex][pmIndex] < min){
        peromonMatrix[vmIndex][pmIndex] = min;
    }
}

function setParameters(_peromonEvapolation, _peromonMinFactor){
    evapolationDegree = _peromonEvapolation;
    minFactor = _peromonMinFactor;
}

module.exports = {
    getMatrix,
    setAll,
    calMax,
    calMin,
    update,
    setParameters
}