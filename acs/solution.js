class Solution {
    constructor(vmList, pmList) {
        this.vmList = JSON.parse(JSON.stringify(vmList));
        this.pmList = JSON.parse(JSON.stringify(pmList));
        this.vmLength = vmList.length;
        this.pmLength = pmList.length;

        this.solutionMatrix = [];
        for(let i=0; i<this.vmLength; i++){
            this.solutionMatrix[i] = [];
            for(let j=0; j<this.pmLength; j++){
                this.solutionMatrix[i][j] = 0;
            }
        }
    }

    //vm을 pm에 할당합니다.
    allocate(vm, pm){
        for(let i = 0; i < this.solutionMatrix[vm.id].length; i++){
            if(this.solutionMatrix[vm.id][i] === 1){
                throw new Error("이미 할당되어 있는 vm을 재할당하려는 시도가 감지되었습니다.");
            }
        }
        this.solutionMatrix[vm.id][pm.id] = 1;
    }

    getAllVmInPm(pm){
        let vmIdList = [];

        for(let i=0; i<this.vmLength; i++){
            if(this.solutionMatrix[i][pm.id] === 1){
                vmIdList.push(i);
            }
        }

        let vmList = [];
        for(let vmId of vmIdList){
            let vm = this.vmList.find(v=>v.id===vmId);
            vmList.push(vm);
        }
        return vmList;
    }

    isBetterThan(anotherSolution){
        let myUsedPmCount = this.usedPmCount();
        let anotherUsedPmCount = anotherSolution.usedPmCount();

        return myUsedPmCount < anotherUsedPmCount;
    }

    isBetterThanOrSame(anotherSolution){
        let myUsedPmCount = this.usedPmCount();
        let anotherUsedPmCount = anotherSolution.usedPmCount();

        return myUsedPmCount <= anotherUsedPmCount;
    }

    usedPmCount(){
        let usedPm = 0;
        for(let pmIndex = 0; pmIndex<this.pmLength; pmIndex++){
            let findFlag = false;

            for(let vmIndex = 0; vmIndex<this.vmLength; vmIndex++){
                if(this.solutionMatrix[vmIndex][pmIndex] === 1){
                    findFlag = true;
                }
            }

            if(findFlag){
                usedPm++;
            }
        }

        return usedPm;
    }

    isContain(vm, pm){
        let vmId = vm.id;
        let pmId = pm.id;

        return this.solutionMatrix[vmId][pmId] === 1;
    }
}

module.exports =  Solution;