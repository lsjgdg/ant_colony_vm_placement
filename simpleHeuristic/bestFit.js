const Solution = require("../acs/solution.js");
const vector = require("../vector/vector.js");

function bestFit(vmList, pmList){
    let leftVmList = JSON.parse(JSON.stringify(vmList));
    let solution = new Solution(vmList, pmList);

    let nowMaxPmIndex = 0;
    for(let vm of leftVmList){
        let minFitness = 999999;//얼마나 해당 pm에 잘 들어맞는지에 대한 수치, 적을수록 넣었을 때 여유공간이 적은 것
        let minFitnessPmIndex = null;
        for(let pmIndex = 0; pmIndex<=nowMaxPmIndex; pmIndex++){
            let testPm = pmList[pmIndex];
            let allocatedVmList = solution.getAllVmInPm(testPm);

            let totalResource = vector.getVector(testPm.capacity.length);
            for(let vm of allocatedVmList){
                totalResource = vector.add(totalResource, vm.resources);
            }
            totalResource = vector.add(totalResource, vm.resources);
            let leftResource = vector.sub(testPm.capacity, totalResource);
            let overflowFlag = false;
            for(let value of leftResource){
                if(value < 0){
                    overflowFlag = true;
                }
            }
            if(overflowFlag){
                continue;
            }
            let fitness = vector.l2Distance(leftResource);

            if(fitness < minFitness){
                 minFitness = fitness;
                 minFitnessPmIndex = pmIndex;
            }
        }

        if(minFitnessPmIndex === null){
            nowMaxPmIndex++;
            let newPm = pmList[nowMaxPmIndex];
            solution.allocate(vm, newPm);
        }
        else {
            solution.allocate(vm, pmList[minFitnessPmIndex]);
        }
    }

    return solution;
}

module.exports = bestFit;