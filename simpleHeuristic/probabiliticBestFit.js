const Solution = require("../acs/solution.js");
const pick = require("pick-random-weighted");
const vector = require("../vector/vector.js");

function probabiliticBestFit(vmList, pmList){
    let copiedVmList = JSON.parse(JSON.stringify(vmList));
    let copiedPmList = JSON.parse(JSON.stringify(pmList));

    let solution = new Solution(copiedVmList, copiedPmList);

    let nowPmIndex = 0;
    let leftVmList = copiedVmList;
    while(leftVmList.length > 0){
        let targetPm = copiedPmList[nowPmIndex];
        let candidateVmList = findCandidateVmList(leftVmList, targetPm, solution);
        if(candidateVmList.length === 0){
            nowPmIndex++;
            continue;
        }

        let pickUpList = [];
        for(let candidateVm of candidateVmList){
            pickUpList.push([candidateVm, heuristricInformation(candidateVm, targetPm, solution)]);
        }

        let selectedVm = pick(pickUpList);
        solution.allocate(selectedVm, targetPm);
        leftVmList = removeItem(leftVmList, selectedVm);
    }

    return solution;
}

function multiProbabiliticBestFit(vmList, pmList, iteration){
    let bestSolution = new Solution(vmList, pmList);

    for(let i = 0; i < iteration; i++){
        let stepSolution = probabiliticBestFit(vmList, pmList);

        if(i === 0){
            bestSolution = stepSolution;
            continue;
        }
        else if(stepSolution.isBetterThan(bestSolution)){
            bestSolution = stepSolution;
        }
    }

    return bestSolution;
}

function heuristricInformation(vm, pm, solution){
    let capacityOfPm = pm.capacity;
    let nowUsedResourcesInPm = totalResources(solution.getAllVmInPm(pm));
    let totalResourceDemand = vector.add(nowUsedResourcesInPm, vm.resources, true);

    let l1Distance = vector.l1Distance(vector.sub(capacityOfPm, totalResourceDemand));

    if(l1Distance === 0){
        return 99999;
    }

    return 1/(l1Distance);
}

function removeItem(vmList, vm){
    let newVmList = [];
    for(let v of vmList){
        if(v.id !== vm.id){
            newVmList.push(v);
        }
    }

    return newVmList;
}

//주어진 vmList에서 pm에 할당 가능한 모든 vm을 구합니다.
function findCandidateVmList(vmList, pm, solution){
    let candiateVmList = [];
    let nowUsedResourcesInPm = totalResources(solution.getAllVmInPm(pm));
    for(let vm of vmList){
        let testResourceVector = arrayDeepCopy(nowUsedResourcesInPm);
        testResourceVector = vector.add(testResourceVector, vm.resources, true);

        let overflowFlag = false;
        for(let i=0; i<testResourceVector.length; i++){
            if(testResourceVector[i] > pm.capacity[i]){
                //단 한 종류의 자원이라도 수용량을 넘었다면
                overflowFlag = true;
            }
        }

        if(overflowFlag === false){
            candiateVmList.push(vm);
        }
    }

    return candiateVmList;
}

//주어진 vm이 요구하는 총합 자원 요구량을 계산
function totalResources(vmList){
    let sum = [];
    //sumvector 초기화
    if(vmList.length > 0){
        for(let i=0; i<vmList[0].resources.length; i++){
            sum.push(0);
        }
    }
    else{
        sum = null;
    }
    for(let vm of vmList){
        sum = vector.add(sum, vm.resources);
    }
    return sum;
}

function arrayDeepCopy(array){
    return JSON.parse(JSON.stringify(array));
}

module.exports = {
    probabiliticBestFit,
    multiProbabiliticBestFit
};