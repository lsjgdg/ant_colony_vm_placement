const Solution = require("../acs/solution.js")
function FFD(vmList, pmList){
    let copiedVmList = JSON.parse(JSON.stringify(vmList));
    let copiedPmList = JSON.parse(JSON.stringify(pmList));
    let solution = new Solution(copiedVmList, copiedPmList);
    let sortedVmList = copiedVmList.sort(function(vm1, vm2){
        let vm1Size = L2Distance(vm1.resources);
        let vm2Size = L2Distance(vm2.resources);
        if(vm1Size < vm2Size){
            return +1;
        }
        else if(vm1Size > vm2Size){
            return -1;
        }
        else {
            return 0;
        }
    }); //내림차순

    let nowPmIndex = 0;
    for(let vm of sortedVmList){
        while(true){
            let candidatePm = copiedPmList[nowPmIndex];
            if(isAllocatable(vm, candidatePm, solution)){
                solution.allocate(vm, candidatePm);
                break;
            }
            else{
                nowPmIndex++;
            }
        }
    }

    return solution;
}

function L2Distance(vector){
    let squareSum = 0;
    for(let element of vector){
        squareSum += (element*element);
    }

    return Math.sqrt(squareSum);
}
function isAllocatable(vm, pm, solution){
    let nowUsedResourcesInPm = totalResources(solution.getAllVmInPm(pm));
    let testResourceVector = arrayDeepCopy(nowUsedResourcesInPm);
    testResourceVector = arraySum(testResourceVector, vm.resources);

    let overflowFlag = false;
    for(let i=0; i<testResourceVector.length; i++){
        if(testResourceVector[i] > pm.capacity[i]){
            //단 한 종류의 자원이라도 수용량을 넘었다면
            overflowFlag = true;
        }
    }

    return !overflowFlag;
}

//주어진 vm이 요구하는 총합 자원 요구량을 계산
function totalResources(vmList){
    let sum = [];
    //sumvector 초기화
    if(vmList.length > 0){
        for(let i=0; i<vmList[0].resources.length; i++){
            sum.push(0);
        }
    }
    else{
        sum = null;
    }
    for(let vm of vmList){
        sum = arraySum(sum, vm.resources);
    }
    return sum;
}

function arrayDeepCopy(array){
    return JSON.parse(JSON.stringify(array));
}

function arraySum(array1, array2) {
    if(array1 === null){
        return array2;
    }
    if(array2 === null){
        return array1
    }
    if(array1.length !== array2.length){
        throw new Error(`array sum 실패, 두 array의 길이가 다릅니다.`);
    }

    let length = array1.length;
    let sumArray = [];

    for(let i = 0; i<length; i++){
        sumArray.push(array1[i] + array2[i]);
    }

    return sumArray;
}

module.exports = FFD;