const mail = require("./mail.js");
const fs = require("fs");

class Logger {
    constructor() {
        this._logList = [];
        this._isPrintConsole = false;
        this._isSendMail = true;
        this._mailTitle = "제목 없는 메일"
        this._peromonHistory = [];
        this._table = null
    }

    isPrintConsole(trueOrFalse){
        this._isPrintConsole = trueOrFalse;
    }

    isSendMail(trueOrFalse){
        this._isSendMail = trueOrFalse;
    }

    setMailTitle(title){
        this._mailTitle = title;
    }

    log(msg){
        this._logList.push(msg);
        if(this._isPrintConsole){
            console.log(msg);
        }
    }

    pushPeromon(peromonMatrix){
        this._peromonHistory.push(JSON.parse(JSON.stringify(peromonMatrix)));
    }

   writeRecode(recode){
        if(this._table === null){
            this._table = [];
        }
        this._table.push(recode);
   }

    async savePeromonHistory(filePath) {
        let fileString = JSON.stringify(this._peromonHistory);
        await writeFile(filePath, fileString);
    }

    async sendAllLog(){
        if(this._isSendMail){
            if(this._table === null){
                await mail.sendMsgList(this._mailTitle, this._logList);
            }
            else {
                await mail.sendMsgListWithTable(this._mailTitle, this._logList, this._table);
            }
        }
    }

    async sendSingleLog(msg){
        if(this._isSendMail){
            await mail.send(this._mailTitle, msg);
        }
    }
}

async function writeFile(filePath, fileString){
    return new Promise(function(res, rej){
        fs.writeFile(filePath, fileString, "utf8", function(error){
            if(error){
                rej(error);
                return;
            }
            res();
        })
    })
}

module.exports = Logger;