const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    auth: {
        user: "admin@syunasoft.com",
        pass: "syung!dg48250"
    },
    host: "mw-002.cafe24.com",
    secure: false,
    port: 25
})

let mail = {
    from: "admin@syunasoft.com",
    to: "gaedageul55@gmail.com",
}


async function send(subject, msg){
    mail.subject = subject;
    mail.html = `<h1>${subject}</h1><br><span>${msg}</span>`;

    try {
        await transporter.sendMail(mail);
        console.info(`${subject}라는 제목으로 메일 전송함.`);
    }catch{
        console.info("메일 전송에 실패함.");
    }
}

async function sendMsgList(subject, msgList){
    mail.subject = subject;
    let html = `<h1>${subject}</h1>`
    for(let msg of msgList){
        html += `<span>${msg}</span><br>`;
    }

    mail.html = html;

    try {
        await transporter.sendMail(mail);
        console.info(`${subject}라는 제목으로 메일 전송함.`);
    }catch{
        console.info("메일 전송에 실패함.");
    }
}

async function sendMsgListWithTable(subject, msgList, table){
    mail.subject = subject;
    let html = `<h1>${subject}</h1>`

    for(let msg of msgList){
        html += `<span>${msg}</span><br>`;
    }

    html += "<span>테이블 데이터</span><br>"
    html += "<table>"

    for(let recode of table){
        html += "<tr>";

        for(let data of recode){
            html += `<td>${data}</td>`;
        }

        html += "</tr>";
    }
    html += "</table>"

    mail.html = html;

    try {
        await transporter.sendMail(mail);
        console.info(`${subject}라는 제목으로 메일 전송함.`);
    }catch{
        console.info("메일 전송에 실패함.");
    }
}

function ignore(){
    isMailSend = false;
}

module.exports = {
    send,
    sendMsgList,
    sendMsgListWithTable,
    ignore
}