function getVector(size){
    let temp = [];
    for(let i = 0; i<size; i++){
        temp[i] = 0;
    }

    return temp;
}

function add(vector1, vector2, nullable = false){
    let sumVector = [];

    if(nullable === true){
        if(vector1 === null){
            vector1 = getVector(vector2.length);
        }
        if(vector2 === null){
            vector2 = getVector(vector1.length);
        }
    }

    if(vector1.length !== vector2.length){
        throw new Error("두 벡터의 길이가 달라 덧셈할 수 없습니다.");
    }

    for(let i=0; i<vector1.length; i++){
        sumVector[i] = vector1[i] + vector2[i];
    }

    return sumVector;
}

// vector1 - vector2
function sub(vector1, vector2){
    let sumVector = [];

    if(vector1.length !== vector2.length){
        throw new Error("두 벡터의 길이가 달라 뺄셈할 수 없습니다.");
    }

    for(let i=0; i<vector1.length; i++){
        sumVector[i] = vector1[i] - vector2[i];
    }

    return sumVector;
}

function l2Distance(vector){
    let squareSum = 0;

    for(let value of vector){
        squareSum += (value*value);
    }

    return Math.sqrt(squareSum);
}

function l1Distance(vector){
    let absVector = abs(vector);
    let sum = 0;

    for(let value of absVector){
        sum += value;
    }

    return sum;
}

function abs(vector){
    let absVector = getVector(vector.length);

    for(let value of vector){
        absVector.push(Math.abs(value));
    }

    return absVector;
}
module.exports = {
    getVector,
    add,
    sub,
    l2Distance,
    l1Distance,
    abs
}