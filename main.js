const Acs = require("./acs/acs.js");
const randomInt = require("random-int");
// const Shufflr = require('shufflr');
const { unsort, unsortInplace } = require("array-unsort");
const FFD = require("./simpleHeuristic/FFD.js");
const bestFit = require("./simpleHeuristic/bestFit.js");
const ProbabiliticBestFit = require("./simpleHeuristic/probabiliticBestFit.js");
const Logger = require("./logger/logger.js");
const path = require("path");

parameterTest();

async function main(){
    const startTime = new Date();
    const testCount = 1;
    const vmCount = 300;
    const capacityVector = [30, 40, 50];
    const iteration = 2;
    const globalLogger = new Logger();
    globalLogger.setMailTitle("vm 128개로 알고리즘 4개간 비교");
    globalLogger.isPrintConsole(true);
    globalLogger.isSendMail(false);

    for(let i = 0; i < testCount; i++){
        try{
            Acs.clearIdState();
            let vmList = randomVmGenerator(vmCount, capacityVector);
            let pmList = [];
            for(let i=0; i<vmList.length; i++) {
                pmList.push(Acs.makePm(capacityVector));
            }

            const logger = new Logger();
            logger.isSendMail(false);
            logger.isPrintConsole(true);

            let timeDict = {
                ffd: {},
                bestFit: {},
                probBestFit: {},
                ant: {}
            };

            timeDict.ffd.start = new Date();
            let ffdSolution = FFD(vmList, pmList);
            timeDict.ffd.end = new Date();

            timeDict.bestFit.start = new Date();
            let bestFitSolution = bestFit(vmList, pmList);
            timeDict.bestFit.end = new Date();

            timeDict.probBestFit.start = new Date();
            let probabiliticBestFitSolution = ProbabiliticBestFit.multiProbabiliticBestFit(vmList, pmList, iteration*5);
            timeDict.probBestFit.end = new Date();

            timeDict.ant.start = new Date();
            let antSolution = await Acs.acs(vmList, pmList, iteration, 5, logger);
            timeDict.ant.end = new Date();

            globalLogger.log(`[제 ${i+1}회차]`)
            globalLogger.log(`FFD: ${ffdSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.ffd.start, timeDict.ffd.end, 2)}초`);
            globalLogger.log(`bestFit: ${bestFitSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.bestFit.start, timeDict.bestFit.end, 2)}초`);
            globalLogger.log(`probabilticBestFit: ${probabiliticBestFitSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.probBestFit.start, timeDict.probBestFit.end, 2)}초`);
            globalLogger.log(`ant: ${antSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.ant.start, timeDict.ant.end, 2)}초`);
            globalLogger.log(``)

            if(i % 10 === 0){
                await globalLogger.sendSingleLog(`${i+1}번 실험이 방금 끝났습니다.`);
            }
        }
        catch(e){
            globalLogger.log(e.stack);
            break;
        }
    }

    globalLogger.log("모든 계산이 끝났습니다.")

    await globalLogger.sendAllLog();
}

function elapsedTimeBySeconds(startTime, endTime, toFixed){
    let elaposedMiliTime = endTime.getTime() - startTime.getTime();
    return Number.parseFloat((elaposedMiliTime/1000).toFixed(toFixed));
}

// 파라미터 테스트용
async function parameterTest(){
    const nCycle = 5;
    const nAnt = 5;
    const vmCount = 128;
    const capacityVector = [20, 30, 40];
    const globalLogger = new Logger();
    globalLogger.setMailTitle("휴리스틱 제곱 지수 조절 실험");
    globalLogger.isPrintConsole(true);
    globalLogger.isSendMail(false);

    const vmList = randomVmGenerator(vmCount, capacityVector);
    const pmList = [];

    for(let i=0; i<vmList.length; i++) {
        pmList.push(Acs.makePm(capacityVector));
    }

    let timeDict = {
        ffd: {},
        bestFit: {},
        probBestFit: {},
        ant: {}
    };

    timeDict.ffd.start = new Date();
    let ffdSolution = FFD(vmList, pmList);
    timeDict.ffd.end = new Date();

    timeDict.bestFit.start = new Date();
    let bestFitSolution = bestFit(vmList, pmList);
    timeDict.bestFit.end = new Date();

    timeDict.probBestFit.start = new Date();
    let probabiliticBestFitSolution = ProbabiliticBestFit.multiProbabiliticBestFit(vmList, pmList, nCycle*nAnt);
    timeDict.probBestFit.end = new Date();

    globalLogger.log("[휴리스틱들의 결과]");
    globalLogger.log(`FFD: ${ffdSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.ffd.start, timeDict.ffd.end, 2)}초`);
    globalLogger.log(`bestFit: ${bestFitSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.bestFit.start, timeDict.bestFit.end, 2)}초`);
    globalLogger.log(`probabilticBestFit: ${probabiliticBestFitSolution.usedPmCount()}개, ${elapsedTimeBySeconds(timeDict.probBestFit.start, timeDict.probBestFit.end, 2)}초`);
    globalLogger.log("");
    globalLogger.log("{여기서부터는 개미의 결과입니다.}");
    globalLogger.log("");

    globalLogger.writeRecode(["peromonSqure", "heuristicSquare", "evapolation", "initMax", "peromon min factor", "avg ant result", "avg time"]);

    let testVector = [0.1, 10.0, 0.1];

    let _testCount = 0;
    for(let heuristicSquareFactor = testVector[0]; heuristicSquareFactor <= testVector[1]; heuristicSquareFactor += testVector[2]) {
        _testCount++;
    }


    const testCount = _testCount;

    console.log(`실험 총 ${testCount}번 할 예정`);

    await globalLogger.sendSingleLog("실험 시작됨...");

    let peromonSquareFactor = 1;
    // let heuristicSquareFactor = 2;
    let peromonEvapolation = 0.7;
    let initMaxPeromon = 3;
    let peromonMinFactor = 2;

    let nowTestNumber = 0;
    for(let heuristicSquareFactor = testVector[0]; heuristicSquareFactor <= testVector[1]; heuristicSquareFactor += testVector[2]) {
        nowTestNumber++;
        let options = {
            peromonSquareFactor,
            heuristicSquareFactor,
            peromonEvapolation,
            initMaxPeromon,
            peromonMinFactor
        }

        try {
            let antSolutionResultList = [];
            let antTimeList = [];

            for(let i=0; i<3; i++){
                const tempLogger = new Logger();
                tempLogger.isSendMail(false);
                tempLogger.isPrintConsole(false);

                timeDict.ant.start = new Date();
                let antSolution = await Acs.acs(vmList, pmList, nCycle, nAnt, tempLogger, options);
                timeDict.ant.end = new Date();

                antSolutionResultList.push(antSolution.usedPmCount());
                antTimeList.push(elapsedTimeBySeconds(timeDict.ant.start, timeDict.ant.end, 2));
            }

            let avgAntResult = (antSolutionResultList[0]+antSolutionResultList[1]+antSolutionResultList[2])/3;
            let avgAntTime = (antTimeList[0] + antTimeList[1] + antTimeList[2]) / 3;

            console.log(`${nowTestNumber}번 끝남`);
            console.log(antSolutionResultList);
            console.log(avgAntResult);

            globalLogger.writeRecode([peromonSquareFactor, heuristicSquareFactor, peromonEvapolation, initMaxPeromon, peromonMinFactor, avgAntResult, avgAntTime]);
        } catch (e) {
            globalLogger.log(e.stack);
            return;
        }
    }

    globalLogger.sendAllLog();
}



function vmGenerator(capacityVector, pmCount){
    let vmList = [];

    for(let p = 0; p<pmCount; p++){
        let leftCapacityVector = capacityVector.slice();//깊은 복사
        while(true){
            let resourceVector = [];
            for(let i = 0; i<leftCapacityVector.length; i++){
                let max = leftCapacityVector[i];
                let rnd = randomInt(0, max);
                resourceVector.push(rnd);
                leftCapacityVector[i] -= rnd;
            }

            //랜덤 뽑기 결과 아무런 자원을 요구하지 않는 VM이면 추가하지 않고 다시 뽑는다.
            let rSum = 0;
            for(let v of resourceVector){
                rSum += v;
            }
            if(rSum > 0){
                vmList.push(Acs.makeVm(resourceVector));
            }

            let sum = 0;
            for(let v of leftCapacityVector){
                sum += v;
            }
            if(sum <= 0){
                break;
            }
        }
    }

    return unsort(vmList);
}

function randomVmGenerator(count, capacityVector){
    let vmList = [];
   for(let i = 0; i < count; i++){
       let resourceVector = [];
       for(let max of capacityVector){
           let rnd = randomInt(1, max);
           // let rnd = 1;
           resourceVector.push(rnd);
       }
       vmList.push(Acs.makeVm(resourceVector));
   }

   return vmList;
}

// console.log(JSON.stringify(solution));